<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class TaskController extends Controller
{
    /**
     * Method to make a request to Giphy API and return view with results.
     * @param SearchRequest $request
     * @Post('searchGif')
     * @return View
     */
    public function searchGif(SearchRequest $request)
    {
        try{
            $client = new Client([
                'base_uri' => 'http://api.giphy.com/v1/',
                'headers' => [
                    'api_key' => env('GIPHY_API_KEY')
                ]
            ]);

            $gifSearchUri = 'gifs/search?q='.$request->search_term.'&limit='.$request->num_of_results;

            $response = $client->request('GET', $gifSearchUri);
            $body = $response->getBody();
            $jsonDecodeBody = json_decode($body);
            $jsonDecodeBodyAry = json_decode($body, true);

            $gifJsonAry = $jsonDecodeBody->data;
            $gifDataAry = $jsonDecodeBodyAry['data'];
        }catch(RequestException $ex){
            $response = $ex->getResponse();
            $errorMessage = 'There was a problem with your search request. Error code: '.$response->getStatusCode();

            return redirect()->back()->withErrors([$errorMessage]);
        }

        if($gifJsonAry && $gifDataAry)
            return view('results', compact('gifDataAry', 'gifJsonAry', 'request'));
        else
            return redirect()->back()->withErrors(['There was a problem with your search request.']);
    }
}
