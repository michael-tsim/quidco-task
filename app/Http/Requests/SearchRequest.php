<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * The route to redirect to if validation fails.
     *
     * @var string
     */
    protected $redirectRoute = 'index';

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validationAry = [
            'search_term' => 'string|nullable|max:25',
            'num_of_results' => 'required|numeric|between:1,25'
        ];

        return $validationAry;
    }

    /**
     * Get the messages to return when a validation rule fails.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'search_term.max' => 'Maximum number of characters exceeded for your search. Maximum number of characters: 25',
            'num_of_results.between' => 'The number of results must be between 1 and 25.',
            'num_of_results.numeric' => 'The number of results must be a number.',
            'num_of_results.required' => 'The number of results is required.'
        ];
    }
}
