<html lang="en">
<head>
	<script src="{{ mix('js/app.js' )}}"></script>
	<link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
	<div class="container my-5">
		<h1 class="text-center">Quidco Task</h1>
		@yield('content')
	</div>
</body>
</html>