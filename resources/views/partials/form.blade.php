<form method="POST" action="{{ route('searchGif') }}">
	@csrf
	<div class="form-group">
		<label for="search_term">Search term: </label>
		<input type="text" name="search_term" class="form-control" value="{{ ($request->search_term ?? '') }}">
	</div>
	<div class="form-group">
		<label for="num_of_results">Number of results: </label>
		<select name="num_of_results" class="form-control">
			@for($x = 1; $x <= 25; $x++)
				<option value="{{ $x }}" {{ (isset($request) && $request->num_of_results == $x ? 'selected' : '') }}>{{ $x }}</option>
			@endfor
		</select>
	</div>
	<button type="submit" class="btn btn-primary">Search</button>
</form>

@if($errors->any())
	<p class="text-danger">{{ $errors->first() }}</p>
@endif