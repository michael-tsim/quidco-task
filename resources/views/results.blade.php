@extends('layout')
@section('content')
	@include('partials.form')

	@if(isset($gifDataAry))
		@if(empty($gifDataAry))
			<p>No results found</p>
		@else
			<table class="table table-responsive mt-5">
				<tr>
					<th>GIF</th>
					<th>JSON Snippet</th>
				</tr>
				@foreach($gifDataAry as $key => $gif)
					<tr>
						<td><img src="https://media.giphy.com/media/{{ $gif['id'] }}/source.gif" width="250"></td>
						<td>
							<span class="toggle-string">Toggle show/hide JSON string</span>
							<div class="json-string mt-3">@json($gifJsonAry[$key])</div>
						</td>
					</tr>
				@endforeach
			</table>
		@endif
	@endif
@endsection