<?php

namespace Tests\Unit;

use Tests\TestCase;

class GifSearchTest extends TestCase
{
    /** 
     * A test to return a successful index page
     *
     * @return void
     * @test 
     */
    public function gettingIndexPage()
    {
        $response = $this->get('/');
        $response->assertSuccessful();
    }

    /** 
     * A test to make a successful search
     *
     * @return void
     * @test 
     */
    public function search()
    {
        $response = $this->post(route('searchGif'), $this->searchAry());

        $response->assertSuccessful();
        $response->assertViewIs('results');
        $response->assertViewHasAll(['gifDataAry', 'gifJsonAry', 'request']);
    }

    /**
     * A test to check form validation for search is working
     *
     * @return void
     * @test
     * @dataProvider searchValidationProvider
     */
    public function searchValidateFields($formInput, $formInputValue)
    {
        $response = $this->post(route('searchGif'), array_merge($this->searchAry(), [$formInput => $formInputValue]));

        $response->assertSessionHasErrors($formInput);
        $response->assertRedirect(route('index'));
    }

    private function searchAry()
    {
        return [
            'search_term' => 'Test',
            'num_of_results' => 5
        ];
    }

    public function searchValidationProvider()
    {
        return [
            ['search_term', 'more than twenty five characters'],
            ['num_of_results', ''],
            ['num_of_results', 'text'],
            ['num_of_results', 0],
            ['num_of_results', 26],
            ['num_of_results', 27]
        ];
    }
}
