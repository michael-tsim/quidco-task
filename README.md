
## Set up

 - `composer install`
 - `npm install`
 - `npm run dev`
 - Copy .env.example to .env `cp .env.example .env`
    - APP_URL=*full app url*
    - GIPHY_API_KEY=*API key generated from "https://developers.giphy.com" (Please contact me if you'd like me to provide my key).*